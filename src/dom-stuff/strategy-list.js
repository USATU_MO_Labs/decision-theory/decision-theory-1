import $ from "jquery";

const strategyListSelector = "#strategy-list";

/**
 * Генератор уникальных id для элементов списка стратегий. На деле просто счетчик.
 */
const getUniqueId = (() => {
  let i = 0;
  return () => i++;
})();

/**
 * Коллбек для обработки кликов по элементам списка стратегий
 * @callback ListItemClickCallback
 * @param {any} itemId уникальный идентификатор элемента списка
 * @returns {void}
 */

/**
 * Создает элемент для списка Bootstrap с данным текстом и коллбеком. Добавляет этот элемент в список. Возвращает уникальный id элемента.
 * @param {any} text текст элемента
 * @param {ListItemClickCallback} callback функция, вызываемая при активации элемента
 * @returns {number} уникальный id стратегии
 */
export function createListItem(text, callback) {
  const itemId = getUniqueId();

  const $item = $(document.createElement("a")).attr({
    "class": "list-group-item list-group-item-action py-1",
    "data-toggle": "list",
    "role": "tab",
    "itemId": itemId
  });

  $(strategyListSelector).append($item);

  $item.text(text.toString());

  if (callback) {
    $item.on("show.bs.tab", e => {
      callback(itemId);
    })
  }

  return itemId;
}

/**
 * Удаляет элемент с данным id из списка. 
 * Эта функция отвечает за работу с DOM. Соответствующее удаляемому элементу состояние необходимо удалять отдельно.
 * @param {number} itemId 
 */
export function removeListItem(itemId) {
  $(strategyListSelector).find(`[itemId="${itemId}"]`).remove();
}

/**
 * Изменяет текст у элемента с данным id в списке.
 * @param {number} itemId 
 * @param {string} newText 
 */
export function rerenderText(itemId, newText) {
  $(strategyListSelector).find(`[itemId="${itemId}"]`).text(newText);
}