import $ from "jquery";

/**
 * Создает столбец с input-ом. Он помещается внутрь ряда.
 */
function createColumn() {
  const $column = $(document.createElement("div")).attr("class", "col");

  const $input = $(document.createElement("input"))
    .attr({
      "class": "form-control form-control-sm my-1",
      "type": "number",
      value: 0
    });

  $column.append($input);

  return $column;
}

/**
 * Создает ряд с n столбцами.
 * @param {number} n количество столбцов
 */
function createRow(n) {
  const $row = $(document.createElement("div")).attr("class", "form-row");

  for (let i = 0; i < n; i++) {
    const $column = createColumn();
    $row.append($column);
  }

  return $row;
}

/**
 * Создает матрицу m*n с input-полями, заполненную нулями, в данной форме. Предварительно чистит форму.
 * @param {JQuery<HTMLElement>} $form jQuery-элемент формы, в которую необходимо поместить матрицу
 * @param {number} m количество строк
 * @param {number} n количество столбцов
 */
export function createEmptyMatrixForm($form, m, n) {
  $form.empty();

  for (let i = 0; i < m; i++) {
    let $row = createRow(n);
    $form.append($row);
  }
}

/**
 * Создает форму с матрицей из данных. Предпологается, что входные данные - прямоугольный двухмерный массив.
 * @param {JQuery<HTMLElement>} $form jQuery-элемент формы, в которую необходимо поместить матрицу
 * @param {number[][]} data данные, из которых делается матрица
 */
export function createMatrixFormFromData($form, data) {
  const m = data.length;
  const n = data[0].length;
  createEmptyMatrixForm($form, m, n);
  fillMatrixWithData($form, data);
}

/**
 * Изменяет размеры матрицы в данной форме. Новые ячейки заполняются нулями. Старые удаляются безвозвратно.
 * Предполагается, что у матрицы следующая структура:
 * div*m > div*n > input
 * Также предполагается, что посторонние элементы отсутствуют.
 * @param {JQuery<HTMLElement} $form форма, содержащая матрицу
 * @param {number} m новое количество строк
 * @param {number} n новое количество столбцов
 */
export function changeMatrixFormDimensions($form, m, n) {
  let $rows = $form.children("div");

  // process rows
  if ($rows.length > m) {
    // remove excess rows
    $rows.each((i, rowElem) => {
      if (i >= m) $(rowElem).remove();
    });
  } 
  
  else if ($rows.length < m) {
    // add rows
    const rowsToAdd = m - $rows.length;
    for (let i = 0; i < rowsToAdd; i++) {
      const $row = createRow(n);
      $form.append($row);
    }
  }

  // process columns
  $rows.each((i, rowElem) => {
    const $row = $(rowElem);

    let $columns = $row.children("div");

    if ($columns.length > n) {
      // remove excess columns
      $columns.each((i, columnElem) => {
        if (i >= n) $(columnElem).remove();
      });
    } 
    
    else if ($columns.length < n) {
      // add columns
      const columnsToAdd = n - $columns.length;
      for (let i = 0; i < columnsToAdd; i++) {
        const $column = createColumn();
        $row.append($column);
      }
    }
  });
}

/**
 * Собирает элементы из формы с матрицей в двухмерный массив.
 * Можно использовать совместно с fillMatrixWithData.
 * @param {JQuery<HTMLElement>} $form элемент-форма, содержащий матрицу
 * @returns {number[][]} ряды * строки
 */
export function gatherDataFromMatrix($form) {
  let matrix = [];

  // rows
  $form.children("div").each((i, rowElem) => {
    let row = [];

    // columns
    $(rowElem).children("div").each((j, colElem) => {
      const value = $(colElem).find("input").prop("value");
      row.push(value);
    });

    matrix.push(row);
  });

  return matrix;
}

/**
 * Заполняет данную форму с матрицей данными данными (тавтология, знаю, не кидайте тапками!).
 * Предполагается, что размер матрицы и размер данных совпадают.
 * Можно использовать совместно с gatherDataFromMatrix.
 * @param {JQuery<HTMLElement>} $form элемент формы, в котором находится матрица
 * @param {number[][]} data данные для подстановки
 */
export function fillMatrixWithData($form, data) {
  // rows
  $form.children("div").each((i, rowElem) => {
    // columns
    $(rowElem).children("div").each((j, colElem) => {
      $(colElem).find("input").prop("value", data[i][j]);
    });
  });
}