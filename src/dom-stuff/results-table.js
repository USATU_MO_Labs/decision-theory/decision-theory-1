/** @module */

import $ from "jquery";

const tableSelector = "#results-table";

/**
 * Очищает таблицу в DOM. Оставляет <table>.
 */
export function clearTable() {
  $(tableSelector).empty();
}

/**
 * Записывает сырую таблицу в DOM. Предпологает, что первые строка и столбец - заголовочные.
 * Преобразует каждый элемент в строку через метод Object.toString(). Специальное форматирование
 * необходимо проводить самостоятельно и заранее.
 * @param {any[][]} table табличные данные, строки * столбцы
 */
function renderRawTable(table) {
  let $table = $(tableSelector);
  let $tableHead = $(document.createElement("thead"));
  let $tableBody = $(document.createElement("tbody"));

  $table.append($tableHead).append($tableBody);

  for (let row_i = 0; row_i < table.length; row_i++) {

    let $rowElem = $(document.createElement("tr"));

    if (row_i === 0) {
      $tableHead.append($rowElem);
    } else {
      $tableBody.append($rowElem);
    }

    let row = table[row_i];

    for (let col_i = 0; col_i < row.length; col_i++) {

      let cellElemTag;
      if (row_i === 0 || col_i === 0) {
        // заголовок таблицы или первый столбец таблицы; все элементы - th
        cellElemTag = "th";
      } else {
        // все остальные элементы; td
        cellElemTag = "td";
      }

      let $cellElem = $(document.createElement(cellElemTag));
      $rowElem.append($cellElem);

      let item = row[col_i];

      $cellElem.text(item.toString());
    }
  }
}

/**
 * Записывает результаты работы алгоритма в специально отведенную для этого таблицу.
 * Группирует по состояниям. Т.е.:
 * - состояние 1 оптимальные стратегии
 * - состояние 1 ожидаемые доходности
 * - состояние 2 оптимальные стратегии
 * - состояние 2 ожидаемые доходности
 * - итд
 * @param {module:markov-stuff.IMarkovSolveResult} solveResult результат работы markovSolve
 */
export function writeToTable(solveResult) {
  // предпологаем, что D и V одинаковых размеров и не проверяем
  // ибо это всего лишь лаба, а не продакшн

  clearTable();

  const D = solveResult.D;
  const V = solveResult.V;

  const N = D.length;
  const stateCount = D[0].length;

  let table = [];

  // header: n, 0, 1..N
  table.push(["n", ...Array(N).keys(), N].map(item => item.toString()));

  // rows: 
  // v_i(n), 0, ...
  // d_i(n), -, ...
  for (let state_i = 0; state_i < stateCount; state_i++) {
    // полные ожидаемые доходности
    const vs = V.map(states => states[state_i]).map(item => item.toFixed(2));
    table.push([`v_${state_i}(n)`, 0, ...vs]);

    // номера оптимальных стратегий
    const ds = D.map(states => states[state_i]).map(item => item.toString());
    table.push([`d_${state_i}(n)`, "-", ...ds]);
  }

  renderRawTable(table);
}