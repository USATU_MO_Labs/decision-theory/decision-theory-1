/**
 * Прослойки между пользовательским интерфейсом (DOM) и алгоритмом
 */

import * as resultsTable from "./results-table";
import * as inputMatrix from "./matrix";
import * as strategyList from "./strategy-list";

export {
  resultsTable,
  inputMatrix,
  strategyList
};