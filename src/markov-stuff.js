/** @module */

import {
  max_i,
  dot
} from "./util";

/**
 * Стратегия
 * @typedef {object} IStrategy
 * @property {number[][]} P матрица вероятностей
 * @property {number[][]} R матрица доходностей
 */

/**
 * Ожидаемая доходность q(i, k)
 * @param {IStrategy} s стратегия
 * @returns {number[]} ожидаемые доходности для каждого состояния
 */
function expectedYield(s) {
  let Q = [];

  for (let i = 0; i < s.P.length; i++) {
    let probabilities = s.P[i];
    let yields = s.R[i];

    let q = dot(probabilities, yields);

    Q.push(q);
  }

  return Q;
}

/**
 * Полная ожидаемая доходность для n-го шага моделирования.
 * n не передается; вместо него передается предыдущее значение ожидаемой доходности.
 * @param {number[]} V_j предыдущие ожидаемые доходности (для каждого состояния)
 * @param {number[][]} Q ожидаемые доходности, стратегии*состояния
 * @param {IStrategy[]} strategies стратегии
 * @returns {{V: number[], D: number[]}} полные ожидаемые доходности и номера оптимальных стратегий для каждого состояния
 */
function fullExpectedYieldAndStratNum(V_j, Q, strategies) {
  // полные ожидаемые доходности для каждого состояния
  let V = [];

  // номера оптимальных стратегий для каждого состояния
  let D = [];

  // for each state
  const statesCount = strategies[0].P.length;
  for (let i = 0; i < statesCount; i++) {
    let v_i_k = [];

    // for each strat
    for (let k = 0; k < strategies.length; k++) {
      // probabilities for state i and strat k
      let probabilities = strategies[k].P[i];

      // let s = sum(probabilities.map(p => p * V_j[i]));
      let s = dot(probabilities, V_j);

      v_i_k.push(s + Q[k][i]);
    }

    // полная ожидаемая доходность для данного состояния
    let v_max_i = max_i(v_i_k);
    V.push(v_i_k[v_max_i]);

    // номер оптимальной стратегии для данного состояния
    D.push(v_max_i);
  }

  return {
    V,
    D
  };
}

/**
 * Результат работы алготитма моделирования дискретного марковского процесса с доходностью на N шагов.
 * @typedef {object} IMarkovSolveResult
 * @property {number[][]} V полные ожидаемые доходности, шаг*состояние
 * @property {number[][]} D номера оптимальных стратегий, шаг*состояние
 */

/**
 * Моделирование дискретного марковского процесса с доходностью на N шагов.
 * @argument {IStrategy[]} strategies список стратегий
 * @argument {number} N количество шагов моделирования
 * @returns {IMarkovSolveResult} полные ожидаемые доходности и номера оптимальных стратегий (V и D соответственно), шаг*состояние
 */
export default function solve(strategies, N) {
  // ожидаемые доходности для каждого состояния; считаются один раз
  let Q = [];
  for (const s of strategies) {
    Q.push(expectedYield(s));
  }

  /**
   * полные ожидаемые доходности и номера оптимальных стратегий для каждого шага
   * @type {{V: number[][], D: number[][]}}
   */
  let result = {
    V: [],
    D: []
  };

  // ожидаемая доходность для каждого состояния на 0 шаге = 0
  let prev_V = strategies[0].P.map(_ => 0);

  for (let i = 0; i < N; i++) {
    let VD = fullExpectedYieldAndStratNum(prev_V, Q, strategies);

    result.V.push(VD.V);
    result.D.push(VD.D);

    // имхо, проще инициализировать это нулями и потом просто ссылаться на 
    // посчитанное значение, нежели дергать это из result и брать нули, если 
    // result.V пуст
    prev_V = VD.V;
  }

  return result;
}