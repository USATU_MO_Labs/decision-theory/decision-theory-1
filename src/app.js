import $ from "jquery";

// bootstrap.css
import "bootstrap/dist/css/bootstrap.min.css";
// bootstrap js
import "bootstrap";

import "./index.css";

import "./dom-binding";

import { init } from "./dom-binding";

$(function() {
  init();

  // выберем первый элемент в списке стратегий
  $("#strategy-list").children(":first-child").tab("show");
});