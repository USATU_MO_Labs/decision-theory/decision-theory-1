/**
 * Лучшие спагетти во всем УГАТУ!
 */

import $ from "jquery";

import {
  resultsTable,
  inputMatrix,
  strategyList
} from "./dom-stuff";

import {
  createMatrix
} from "./util";

import markovSolve from "./markov-stuff";


// параметры к алгоритму; начальное значение - тестовый пример
let paramsState = {
  stateCount: 2,
  strategyCount: 3,

  selectedStrategyId: null,

  strategies: [{
      uniqueId: null, // will be filled
      name: "strat 1",
      P: [
        [1, 0],
        [0.1, 0.9],
      ],
      R: [
        [8.68, 0],
        [2.43, 3.29],
      ],
    },

    {
      uniqueId: null, // will be filled
      name: "strat 2",
      P: [
        [1, 0],
        [0.33, 0.67],
      ],
      R: [
        [16.83, 0],
        [14.11, 7.63],
      ],
    },

    {
      uniqueId: null, // will be filled
      name: "strat 3",
      P: [
        [1, 0],
        [0.33, 0.67],
      ],
      R: [
        [3.23, 0],
        [10.07, 7.86],
      ],
    },
  ]
};

function getSelectedStrategy() {
  let strategy = paramsState.strategies.find(strategy => strategy.uniqueId == paramsState.selectedStrategyId);

  if (!strategy) {
    console.error(`Unable to find strategy with id "${strategyId}!"`);
  };

  return strategy;
}

function onStrategyNameInput(e) {
  // почему-то оно не работает; при изменении input.value из кода новое значение не отображается в браузере. Отображается только старое. Если начать его менять, измененное старое окажется в input.value
  const name = e.target.value;
  let strategy = getSelectedStrategy();

  if (strategy) {
    strategy.name = name;
    strategyList.rerenderText(paramsState.selectedStrategyId, name);
  }
}

function onStateCountApply() {
  const newStateCount = parseInt($("#input-state-count").prop("value"), 10);

  // don't run if don't need to
  if (newStateCount === paramsState.stateCount) return;

  console.log(`Changing state count from ${paramsState.stateCount} to ${newStateCount}`);

  // update state count
  paramsState.stateCount = newStateCount;

  // resize probability and yield matrices
  paramsState.strategies.map(strategy => {

    for (let mat of [strategy.P, strategy.R]) {

      // remove excess rows
      if (mat.length > newStateCount) {
        mat.splice(newStateCount, mat.length - newStateCount);
      }

      // add new rows
      else if (mat.length < newStateCount) {
        let rowsToAdd = newStateCount - mat.length;
        for (let i = 0; i < rowsToAdd; i++) {
          let row = (new Array(newStateCount)).fill(0);
          mat.push(row);
        }
      }

      // process columns
      for (let row of mat) {
        // remove excess elements
        if (row.length > newStateCount) {
          row.splice(newStateCount, row.length - newStateCount);
        }

        // add new elements
        else if (row.length < newStateCount) {
          let extra = (new Array(newStateCount - row.length)).fill(0);
          row.push(...extra);
        }
      }
    }
  });

  // resize matrix forms
  inputMatrix.changeMatrixFormDimensions($("#form-probability-matrix"), newStateCount, newStateCount);
  inputMatrix.changeMatrixFormDimensions($("#form-yield-matrix"), newStateCount, newStateCount);
}

function onStrategyCountApply() {
  const newStrategyCount = parseInt($("#input-strategy-count").prop("value"), 10);

  if (newStrategyCount === paramsState.strategyCount) return;

  console.log(`Changing strategy count from ${paramsState.strategyCount} to ${newStrategyCount}`);

  // remove excess strategies
  if (newStrategyCount < paramsState.strategyCount) {
    let removedStrategies = paramsState.strategies.splice(newStrategyCount, paramsState.strategyCount - newStrategyCount);

    for (let strategy of removedStrategies) {
      strategyList.removeListItem(strategy.uniqueId);
    }
  }

  // add new strategies
  else {
    const strategiesToAdd = newStrategyCount - paramsState.strategyCount;

    for (let i = 0; i < strategiesToAdd; i++) {
      let name = "New strategy";
      let uniqueId = strategyList.createListItem(name, bindStrategyToForms);

      let strategy = {
        name,
        uniqueId,
        P: createMatrix(paramsState.stateCount, paramsState.stateCount, 0),
        R: createMatrix(paramsState.stateCount, paramsState.stateCount, 0)
      };

      paramsState.strategies.push(strategy);
    }
  }

  // update strategy count
  paramsState.strategyCount = newStrategyCount;
}

function bindStrategyToForms(strategyId) {
  paramsState.selectedStrategyId = strategyId;
  let strategy = getSelectedStrategy();

  $("#input-strategy-name").prop("value", strategy.name);
  inputMatrix.fillMatrixWithData($("#form-probability-matrix"), strategy.P);
  inputMatrix.fillMatrixWithData($("#form-yield-matrix"), strategy.R);
}

function onStartClick() {
  const stepCount = parseInt($("#input-step-count").prop("value"), 10);
  let result = markovSolve(paramsState.strategies, stepCount);
  resultsTable.writeToTable(result);
}

export function init() {
  // начальное состояние
  $("#input-state-count").prop("value", paramsState.stateCount);
  $("#input-strategy-count").prop("value", paramsState.strategyCount);

  const $probabilityMatrix = $("#form-probability-matrix");
  const $yieldMatrix = $("#form-yield-matrix");

  // инициализация матриц
  inputMatrix.createEmptyMatrixForm($probabilityMatrix, paramsState.stateCount, paramsState.stateCount);
  inputMatrix.createEmptyMatrixForm($yieldMatrix, paramsState.stateCount, paramsState.stateCount);

  // изменение состояния при изменении форм-матриц
  $probabilityMatrix.on("change", () => {
    let strategy = getSelectedStrategy();
    strategy.P = inputMatrix.gatherDataFromMatrix($probabilityMatrix);
  });
  $yieldMatrix.on("change", () => {
    let strategy = getSelectedStrategy();
    strategy.R = inputMatrix.gatherDataFromMatrix($yieldMatrix);
  });

  // создание стратегий в списке и добавление id к начальному состоянию
  paramsState.strategies.map(strategy => {
    const uniqueId = strategyList.createListItem(strategy.name, bindStrategyToForms);
    strategy.uniqueId = uniqueId;
  });

  // обработчик изменения названия текущей выбранной стратегии
  $("#input-strategy-name").on("input", onStrategyNameInput);

  // обработчики изменений количества состояний и стратегий
  $("#button-state-count-apply").on("click", onStateCountApply);
  $("#button-strategy-count-apply").on("click", onStrategyCountApply);

  // запуск рассчета
  $("#button-start").on("click", onStartClick);
}
