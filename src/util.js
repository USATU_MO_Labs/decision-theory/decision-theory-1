import { createMatrixFormFromData } from "./dom-stuff/matrix";

// наверное, все это можно заменить lodash-ем, но как-то пофиг

/**
 * Сумма
 * @param {number[]} arr 
 * @returns {number}
 */
// костыль, потому что в js нет своего sum
export function sum(arr) {
  return arr.reduce((total, curr) => total + curr, 0);
}

/**
 * Индекс максимального элемента, или -1, если массив пустой
 * @param {number[]} arr 
 * @returns {number}
 */
export function max_i(arr) {
  let m_i = -1;
  for (let i = 0; i < arr.length; i++) {
    if (m_i == -1) {
      m_i = i;
    } else {
      if (arr[m_i] < arr[i]) m_i = i;
    }
  }

  return m_i;
}

/**
 * Скалярное произведение
 * @param {number[]} arr_a 
 * @param {number[]} arr_b 
 * @returns {number}
 */
export function dot(arr_a, arr_b) {
  if (arr_a.length != arr_b.length) {
    throw new Error(`Unequal array lengths ${arr_a.length} and ${arr_b.length}!`);
  }

  let s = 0;
  for (let i = 0; i < arr_a.length; i++) {
    s += arr_a[i] * arr_b[i];
  }

  return s;
}


export function createMatrix(m, n, filler) {
  let matrix = [];
  for (let i = 0; i < m; i++) {
    let row = [];
    for (let j = 0; j < n; j++) {
      row.push(filler);
    }
    matrix.push(row);
  }
  return matrix;
}