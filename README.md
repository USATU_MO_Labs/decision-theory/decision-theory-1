# decision-theory-1

Теория принятия решений. Лаба 1.

# Что надо

Установленный node. И всё.

# Как пользоваться

Клонировать
```
git clone git@gitlab.com:USATU_MO_Labs/decision-theory-1.git
```

Установить зависимости
```
cd decision-theory-1
npm install
```

Запустить
```
npm start
```

Будет запущен webpack-dev-server по адресу localhost:8080 (адрес, если вдруг что, выводится в консоли).

Там будет рабочее приложение.

# Из чего сделано

Говно и палки! А именно: 

- Cytoscape для графов
- jQuery для сбора пользовательского ввода из DOM, показа динамических данных и для Bootstrap
- Bootstrap для стилей и виджетов
- Webpack + Babel для сборки Javascript ES6 и упаковки в готовый для сервировки бандл
